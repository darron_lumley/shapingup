(function () {
    angular.module('ShapingUp').controller('StoreController', ['$http', StoreController]);

    function StoreController($http) {
        var me = this;
        me.products = [];

        $http.get('products.json').success(function(data){
            me.products = data;
        });
    }
})();
