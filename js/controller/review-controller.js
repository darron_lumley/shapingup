(function(){
    angular.module('ShapingUp').controller('ReviewController', ReviewController);

    function ReviewController(){
        var me = this;
        me.review = {};
        me.addReview = function(product){
            me.review.createdOn = Date.now();
            product.reviews.push(me.review);
            me.review = {};
        }
    }
})();