(function() {
    angular.module('ShapingUp').controller('GalleryController', GalleryController);

    function GalleryController(){
        var me = this;
        me.current = 0;
        me.setCurrent = function(value){
            me.current = value || 0;
        };
    }
})();