(function () {
    angular.module('ShapingUp').controller('PanelController', PanelController);

    function PanelController() {
        var me = this;
        me.tab = 1;

        me.selectTab = function (setTab) {
            me.tab = setTab;
        };
        me.isSelected = function (checkTab) {
            return me.tab === checkTab;
        }
    }
})();