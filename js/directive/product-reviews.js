(function(){
    angular.module('ShapingUp').directive('productReviews', ProductReviews);

    function ProductReviews(){
        return {
            restrict: 'E',
            templateUrl: 'product/product-reviews.html'
        }
    }
})();