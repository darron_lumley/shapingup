(function(){
    angular.module('ShapingUp').directive('productSpecification', ProductSpecification);

    function ProductSpecification(){
        return {
            restrict: 'E',
            templateUrl: 'product/product-specification.html'
        }
    }
})();

