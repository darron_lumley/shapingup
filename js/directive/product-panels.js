(function(){
    angular.module('ShapingUp').directive('productPanels', ProductPanels);

    function ProductPanels(){
        return {
            restrict: 'E',
            templateUrl: 'product/product-panels.html',
            controller: 'PanelController',
            controllerAs: 'panel'
        }
    }
})();