(function(){
    angular.module('ShapingUp').directive('productGallery', ProductGallery);

    function ProductGallery(){
        return {
            restrict: 'E',
            templateUrl: 'product/product-gallery.html',
            controller: 'GalleryController',
            controllerAs: 'gallery'
        }
    }
})();