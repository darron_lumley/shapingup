(function(){
    angular.module('ShapingUp').directive('productDescription', ProductDescription);

    function ProductDescription() {
        var me = this;
        return {
            restrict: 'E',
            templateUrl: 'product/product-description.html'
        };
    }
})();